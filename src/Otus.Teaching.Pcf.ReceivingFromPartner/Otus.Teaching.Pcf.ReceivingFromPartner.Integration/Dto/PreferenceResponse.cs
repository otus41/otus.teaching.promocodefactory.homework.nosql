﻿using System;

namespace Otus.Teaching.Pcf.ReceivingFromPartner.Integration.Dto
{
    /// <summary>
    /// Элемент справочника.
    /// </summary>
    public class PreferenceResponse
    {
        /// <summary>
        /// Идентификатор.
        /// </summary>
        public Guid Id { get; set; }
        
        /// <summary>
        /// Наименование.
        /// </summary>
        public string Name { get; set; }
    }
}