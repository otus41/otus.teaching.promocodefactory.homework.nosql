﻿using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Gateways
{
    public interface IReferenceBookGateway
    {
        /// <summary>
        /// Получить справочные дынные.
        /// </summary>
        Task<IEnumerable<Preference>> GetPreferencesAsync();
    }
}