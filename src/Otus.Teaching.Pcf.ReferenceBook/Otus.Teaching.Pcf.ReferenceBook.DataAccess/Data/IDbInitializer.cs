﻿namespace Otus.Teaching.Pcf.ReferenceBook.DataAccess.Data
{
    public interface IDbInitializer
    {
        void InitializeDb();
    }
}